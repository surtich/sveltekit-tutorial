import { error } from '@sveltejs/kit';

/** @type {import('./$types').PageLoad} */
export function load({ params }) {
	if (!['blue', 'red', 'green'].includes(params.color)) {
		throw error(400, { message: 'bad color' });
	}
	return {
		color: params.color
	};
}
